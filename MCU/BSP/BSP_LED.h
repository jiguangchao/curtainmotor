
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BSP_LED_H__
#define __BSP_LED_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */
#include "platform.h"

/**
 * @brief
 * @param
 * @retval None
 */
void BSP_LEDON_RED(void);
/**
 * @brief
 * @param
 * @retval None
 */
void BSP_LEDOFF_RED(void);











#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
